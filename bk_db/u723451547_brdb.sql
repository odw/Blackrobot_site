
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 22 2016 г., 14:53
-- Версия сервера: 10.0.20-MariaDB
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u723451547_brdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `br_news`
--

CREATE TABLE IF NOT EXISTS `br_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail_url` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `content_url` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `br_news`
--

INSERT INTO `br_news` (`id`, `title`, `thumbnail_url`, `content`, `content_url`, `date`) VALUES
(1, 'C 14 по 16 августа 2016г в Одессе прошел турнир Пороховая Башня', 'images/news/2016/08/17/tower_of_powder_title_thumb.jpg', '  <table  style="width: 100%;"  border="0">\n        <tbody>\n          <tr>\n            <td></td>\n          </tr>\n                    <tr>\n            <td>\n                <ul> \n                <li>Состязания по средневековому бою между представителями разных рыцарских «дворов»;</li>\n                <li>Ярмарка предметов народного промысла;</li>\n                <li>Мастер-классы по кузнечному и гончарному делу;</li>\n                <li> Разнообразная кухня и всевозможные напитки. Блюда, приготовленные по старинным рецептам;</li>\n                <li>Конурсы и квесты на историческую тематику;</li>\n                <li>Выступления актёров бродячего цирка;</li>\n                <li>Мастер-классы по средневековому танцу;</li>\n                <li>Фотосессии в рыцарских доспехах и со средневековым оружием;</li>\n                <li>Стрельба из лука и арбалета;</li>\n                <li>Конкурс-показ средневековых костюмов</li>\n                <li> Вечерние выступления коллективов фольклорной музыки и танцевальные конкурсы</li>\n                </ul>\n            </td>            \n          </tr>\n                    <tr>\n            <td><a href="http://odesa-kju-spm.16mb.com/foliogallery/foliogallery-tower_of_powder_2016_08.php">Фотоотчет</a></td>\n          </tr>\n        </tbody>\n      </table>', '', '2016-08-17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
