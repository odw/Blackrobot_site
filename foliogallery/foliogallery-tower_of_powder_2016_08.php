<!DOCTYPE html>
<head>
<title>Image Gallery By FolioPages.com</title>
<style type="text/css">
body {
background:#eee;
margin:0;
padding:0;
font:12px arial, Helvetica, sans-serif;
color:#222;
}
</style>
<link type="text/css" rel="stylesheet" href="foliogallery/foliogallery.css" />
<link type="text/css" rel="stylesheet" href="colorbox/colorbox.css" />
<script type="text/javascript" src="foliogallery/jquery.1.11.js"></script>
<script type="text/javascript" src="foliogallery/foliogallery.js"></script>
<script type="text/javascript" src="colorbox/jquery.colorbox-min.js"></script>
</head>
<body>
<div id="folioGallery1" class="folioGallery" title="tower_of_powder_2016_08"><div class="numPerPage" title="3"></div></div>
</body>
</html>